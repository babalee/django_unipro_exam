from django.urls import path
from .views import *

app_name = 'employer'

urlpatterns = [

    # Employer URLS

    path('employer/', EmployerList.as_view(), name='employer_list'),
    path('employer/add', EmployerCreate.as_view(), name='employer_add'),
    path('employer/<int:pk>/', EmployerDetail.as_view(), name='employer_details'),
    path('employer/edit/<int:pk>/', EditEmployer.as_view(), name='employer_edit'),
    path('employer/delete/<int:pk>/', DeleteEmployer.as_view(), name='employer_delete'),
    path('employer/search/', EmployerListDetailFilter.as_view(), name='employer_search'),

    # Departement URLS

    path('departement/', DepartementList.as_view(), name='departement_list'),
    path('departement/add', DepartementCreate.as_view(), name='departement_add'),
    path('departement/<int:pk>/', DepartementDetail.as_view(), name='departement_details'),
    path('departement/edit/<int:pk>/', EditDepartement.as_view(), name='departement_edit'),
    path('departement/delete/<int:pk>/', DeleteDepartement.as_view(), name='departement_delete'),
    path('departement/search/', DepartementListDetailFilter.as_view(), name='departement_search'),

    
]
